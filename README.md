# `hfctl` – A simple CLI utility to manage `/etc/hosts`

`hfctl` is a simple command line utility to add, update, and remove entries in `/etc/hosts`. It can be used along with a configuration file called `.hfctlrc` in the directory of execution (working directory):

```bash
tree -a -L 1
.
├── .hfctlrc
├── README.md
└── WHATEVER

hfctl
>>> Successfully updated '127.0.0.1 exmple.com'
>>> Successfully updated '127.0.0.1 www.example.com'
```

***If you run `hfctl` without options, the programm tries add, or update the entries in `/etc/hosts` based on your `.hfctlrc`-file.***

## Installation

```bash
curl https://gitlab.com/meengit/hfctl/-/raw/master/hfctl.sh --output /usr/local/bin/hfctl
sudo chmod +x /usr/local/bin/hfctl
```

I assume you have `/usr/local/bin` in your `$PATH`.

## .hfctlrc

**Format**

* IP address and hostname, separated by one space.
* One combination of IP address and hostname per line.

You can find an example file <a href="https://gitlab.com/meengit/hfctl/-/blob/master/.hfctlrc" target="_blank">here</a>.

## Usage

Add/Update `/etc/hosts` by `.hfctlrc` in current working directory:

```bash 
hfctl
```

Add IP address and hostname manually (and without `.hfctlrc`):

```bash
hfctl -a "127.0.0.1 www.example.com"
```

Remove IP address and hostname manually (and without `.hfctlrc`):

```bash
hfctl -r www.example.com
```

Remove entries of `.hfctl` form `/etc/hosts`:

```bash
hfctl -R
```

Get help:

```bash
hfctl -h
```

## Ducmentation

Please see `docs/html` (offline) or <https://meengit.gitlab.io/hfctl/> (online).

## License

Please see <https://gitlab.com/meengit/hfctl/-/blob/master/LICENSE>

## Changelog

Please see <https://gitlab.com/meengit/hfctl/-/blob/master/CHANGELOG.md>
