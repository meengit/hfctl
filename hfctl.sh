#!/usr/bin/env bash

#===============================================================================
#
## @file
## @author Andreas
## @brief A simple bash script to manage /etc/hosts
## @copyright (c) 2020-present: Andreas; License: MIT
## @version 0.1.0-alpha
#
# USAGE:       ./hfctl.sh (script), hfctl (global installed command line interface)
#
# OPTIONS:      Please use the man page 'hfctl -h'
# REQUIREMENTS: echo, getopts, groff, grep, less, printf, pwd, read, sed, shift, 
#               tee, tr
# BUGS:         Please see https://gitlab.com/meengit/hfctl/-/issues
# NOTES:        – Line comments using two sharps (#) at the beginning 
#                 are captured by "bash-doxygen."
#               – Initially forked from Fuxy22
#                 https://gist.github.com/Fuxy22/da4b7ca3bcb0bfea2c582964eafeb4ed
# CREATED:      Please use the man page 'hfctl -h'
# REVISION:     Please use the man page 'hfctl -h'
#===============================================================================

# Debugging
# set -o nounset                             # Treat unset variables as an error
# set -x                                     # Debug when it executs

# Env. config
set -o posix

# Global Constants

declare -r CRED=$(tput setaf 1)
declare -r CGREEN=$(tput setaf 2)
declare -r CYELLOW=$(tput setaf 3)
declare -r CBLUE=$(tput setaf 4)
declare -r CRESET=$(tput sgr0)
declare -r DEFAULTMSG="Please run 'hfctl -h' to see how to use 'hfctl'.\\n"

## @fn help()
## @brief Display help (as man page).
## 
function help() {
echo '
.\" Manpage for hfctl.
.\" Contact development@meen.ch to correct errors or typos.
.TH man 7 "24 Oct 2020" "0.1.0-alpha" "hfctl man page"
.SH NAME
hfctl \- command line utility to manage entries /etc/hosts
.SH SYNOPSIS
hfctl [-arR] [-a "hostname ip"] [-r hostname] [-R]
hfctl -h
.SH DESCRIPTION
hfctl is a simple command line utility to add, update, and remove entries in /etc/hosts.

hfctl can be used with a configuration file called ".hfctlrc" in the directory of execution
(working directory). If you run hfctl without options, it tries to find, add, or update 
the entries of your ".hfctlrc"-file.
.SH OPTIONS
.TP
If no option is given: Add/update /etc/hosts with entries in .hfctlrc.
.TP
-a "hostname ip"
Add hostname and ip to /etc/hosts. Hostname and ip must be quoted as a string!
.TP
-r hostname
Remove given hostname from /etc/hosts
.TP
-R
Remove entries in .hfctlrc

.SH SEE ALSO
Possible similar projects: https://github.com/xwmx/hosts, https://github.com/guumaster/hostctl
.SH BUGS
Please see https://gitlab.com/meengit/hfctl/-/issues
.SH AUTHOR
Andreas (development@meen.ch)
' | groff -t -e -mandoc -Tascii /dev/stdin | less
}

## @fn add()
## @brief Add entries to /etc/hosts.
## @param $1 IP address
## @param $2 Hostname
##
function add() {
  if [ -n "$1" ] && [ -n "$2" ]
  then
    printf "%s\t%s\n" "$1" "$2" | sudo tee -a /etc/hosts > /dev/null;
  fi

  if [ -z "$(grep "$2" /etc/hosts)" ]
  then
      echo "${CRED}>>> ERROR: Failed to add $2, try again!\\n ${CRESET}";
      exit 1
  fi
}

## @fn remove()
## @brief Remove entries /etc/hosts by hostname.
## @param $1 Hostname
##
function remove() {
  local hostname=$1

  if [ -n "$(grep $hostname /etc/hosts)" ]
  then
    sudo sed -i".bak" "/$hostname/d" /etc/hosts
  else
    echo "${CYELLOW}>>> WARNING: $hostname was not found in your '/etc/hosts'.\\n ${CRESET}";
    exit 1
  fi
}

## @fn update()
## @brief Update entries in /etc/hosts (only in context with .hfctlrc config file).
## @param $1 IP address
## @param $2 Hostname
##
function update() {
  if [ -n "$(grep "$2" /etc/hosts)" ]
    then
      remove "$2"
      add "$1" "$2"
  else
    add "$1" "$2"
  fi
}

## @fn finalView()
## @brief View notifies the user about a successful execution.
## @param $1 IP address
## @param $2 Hostname
## @param $3 Operation
##
function finalView() {
  if ! [[ $1 =~ ^[0-9]+$ ]]
  then
    echo ">>> Successfully $3 '$1 $2'"
  else
    echo ">>> Successfully $3 '$2'"
  fi
}

## @fn hostsfileController()
## @brief Controller managing operations on /etc/hosts.
## @param $1 IP address
## @param $2 Hostname
## @param $3 Integer (0, 1) or unset
##
function hostsfileController() {
  local status=0

  local ipaddr="$1"
  local hostname="$2"

  if [ -z "$3" ] && ! [[ $2 =~ ^[0-9]+$ ]]
    then
      status=1
      update "$ipaddr" "$hostname"
      finalView "$1" "$2" "updated"
  fi

  if [[ $3 =~ ^[0-9]+$ ]]
  then
    if [ "$3" -eq 0 ]
    then
      add "$1" "$2"
      status=1
      finalView "$1" "$2" "added"
    fi
    if [ "$3" -eq 1 ]
    then
      remove "$2"
      status=1
      finalView "$1" "$2" "removed"
    fi
  fi

  if [ "$status" -eq 0 ]
  then
    echo "${CRED}>>> ERROR: Wrong arguments! $DEFAULTMSG ${CRESET}"
    exit 1;
  fi
}

## @fn configFileController()
## @brief Controller handling .hfctlrc config file.
## @param $1 Integer (0, 1) or unset
##
function configFileController() {
  local action="$1"
  if [ -f "$(pwd | tr -d '\n')/.hfctlrc" ]
  then
    while IFS= read -r line || [ -n "$line" ]; do
    if [ -z $action ] || [ $action -le 1 ]
    then
      hostsfileController $line $action
    else
      hostsfileController $line
    fi
    done < $(pwd | tr -d '\n')/.hfctlrc
  else
      echo "${CYELLOW}>>> WARNING: No parameter given \
and no config file '.hfctlrc' found. ${CRESET}
${DEFAULTMSG}"
    fi
}

## @fn mainController()
## @brief Script controller, loads functions by given options.
## @param "$@" parameters were passed
##
function mainController() {
    while getopts ":a:hr:R" opt; do
    case $opt in
      a)
        hostsfileController $OPTARG 0
        ;;
      h)
        help
        ;;
      r)
        hostsfileController 0 $OPTARG 1
        ;;
      R)
        echo ">>> Removing entries defined in file '.hfctlrc'."
        configFileController 1
        ;;
      \?)
        echo "${CRED}>>> Error: Invalid option: -$OPTARG. ${CRESET}" >&2
        exit 1
        ;;
      :)
        echo "${CRED}>>> Error: Option -$OPTARG requires an argument. ${CRESET}" >&2
        exit 1
        ;;
    esac
  done

  if [ $OPTIND -eq 1 ]; then 
    configFileController
    shift $((OPTIND-1))
    exit 1
  fi
}

## @fn hfctl()
## @brief Main function.
## @param "$@" parameters were passed
##
function hfctl() {
  mainController "$@"
}

hfctl "$@"
